<?

$listaAdm = new Administradora();
$result = $listaAdm->getAdm($_GET['id']);
$adm = $result['resultSet'];

?>

<h1 class="text-center mb-5">Cadastros das Administradoras</h1>
<div class="row">
    <div class="col-12">

        <form action="" method="post" id="formAdm">

            <div class="form-group col-md-6 d-inline-block">
                <label for="nomeAdm">Nome da Administradora*</label>
                <input type="text" name="nomeAdm" class="form-control" id="nomeAdm" aria-describedby="nomeAdm" value="<?=$adm['nomeAdm']?>" required>
            </div>
            
            <div class="form-group col-12 col-md-3 d-inline-block">
                <label for="qtdeCond">Quantidade de condomínios*</label>
                <input type="text" name="qtdeCond" class="form-control" id="qtdeCond" aria-describedby="qtdeCond" value="<?=$adm['qtdeCond']?>" required>
            </div>

            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <button type="submit" class="btn btn-dark btnEnviar col-12 col-sm-1 ml-3 mb-3">Enviar</button>
            <a href="index.php?page=consultaAdm" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar administradoras"><i class="bi bi-journal-text" style="font-size: 2rem"></i></a>
        </form>
    </div>
</div>