<?

$listaBlocos = new cadBlocos();
$result = $listaBlocos->getBlocos($_GET['id']);
$blocos = $result['resultSet'];

?>

<h1 class="mb-4 mt-3 text-center">Cadastro de Blocos</h1>

<div class="row">
    <div class="col-12">
        <form action="" method="post" id="formBlocos">

            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="from_condBloco" style="width: 100%; text-align: center">Condomínio*</label>
                <div class="col-12 col-md-12">
                    <select name="from_condBloco" id="from_condBloco" class="custom-select" style="width:30%; margin: 0 23rem">
                        <option value="">Select</option>
                        <?
                        $condominio = new CadCondominio();
                        $result = $condominio->getCondominio();
                        $cond = $result['resultSet'];

                        foreach($cond as $ch=>$valor){?>
                            <option value="<?=$valor['id']?>" <?=($valor['id'] == $blocos['from_condBloco'] ? 'selected="selected"' : '') ?>><?=$valor['nomeCond']?></option> 
                        <?}?>

                    </select>
                </div>
            </div>

            <div class="form-group col-12 text-center" style="width: 100%; ">
                <label for="nomeBloco">Nome do bloco*</label>
                <input type="text" name="nomeBloco" class="form-control" id="nomeBloco" value="<?=$blocos['nomeBloco']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center " style="width: 100%;">
                <label for="qtdeAndares">Quantidade de andares*</label>
                <input type="text" name="qtdeAndares" class="form-control" id="qtdeAndares" value="<?=$blocos['qtdeAndares']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="qtdeUni">Quantidade de unidades por andar*</label>
                <input type="text" name="qtdeUni" class="form-control" id="qtdeUni" value="<?=$blocos['qtdeUni']?>" style="width:50%; margin: 0 270px" require>
            </div>
        
            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>
                
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-dark btnEnviar col-12 ml-3 mb-3" style="width: 10%;">Enviar</button>
                <a href="index.php?page=consultaBlocos" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar blocos"><i class="bi bi-journal-text" style="font-size: 2rem"></i></a>
            </div>
        </form>
    </div>
</div>