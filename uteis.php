<?
session_start();
error_reporting(0);
require "classes/ConnectDB.class.php";
require "classes/dao.class.php";
require "classes/control.Class.php";
require "classes/administradora.Class.php";
require "classes/cadCondominio.Class.php";
require "classes/cadBlocos.Class.php";
require "classes/cadUnidades.Class.php";
require "classes/cadastro.Class.php";
require "classes/conselhoFiscal.php";

// set_error_handler('trataErros');

// function trataErros($errorNum, $errorStr, $errorFile, $errorLine){
//     echo 'Numero '.$errorNum.' x '.'String: '.$errorStr.' x '.'File: '.$errorFile.' x '.'Linha: '.$errorLine;
// }

function dateFormat($d, $tipo = true){ //2022-03-23 16:24:37

    if(!$d){
        return '--';
    }

    if($tipo){
        $hora = explode(' ', $d); //divide em dia[0] e hora[1]
        $data = explode('-', $hora[0]);
        return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];

    } else{

        $hora = explode(' ', $d);
        $data = explode('/', $hora[0]);
        return $data[2].'-'.$data[1].'-'.$data[0].' '.$hora[1];
    }
}

function legivel($var,$width = '250',$height = '400') {
    echo "<pre>";
    if(is_array($var)) {
        print_r($var);
    } else {
        print($var);
    }
    echo "</pre>";
}

$estados = array( 
    "AC" => "Acre", 
    "AL" => "Alagoas", 
    "AM" => "Amazonas", 
    "AP" => "Amapá",
    "BA" => "Bahia",
    "CE" => "Ceará",
    "DF" => "Distrito Federal",
    "ES" => "Espírito Santo",
    "GO" => "Goiás",
    "MA" => "Maranhão",
    "MT" => "Mato Grosso",
    "MS" => "Mato Grosso do Sul",
    "MG" => "Minas Gerais",
    "PA" => "Pará",
    "PB" => "Paraíba",
    "PR" => "Paraná",
    "PE" => "Pernambuco",
    "PI" => "Piauí",
    "RJ" => "Rio de Janeiro",
    "RN" => "Rio Grande do Norte",
    "RO" => "Rondônia",
    "RS" => "Rio Grande do Sul",
    "RR" => "Roraima",
    "SC" => "Santa Catarina",
    "SE" => "Sergipe",
    "SP" => "São Paulo",
    "TO" => "Tocantins"
);


?>