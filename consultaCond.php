<?
$condominio = new CadCondominio();
$condominio->pagination=3;
$result = $condominio->getCondominio();
?>

<h1 class="text-center mb-4">Consulta dos condomínios</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="index.php?page=cadCondominio" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>
        <table class="table text-center" id="listaCond">
            <thead>
                <tr>
                    <th scope="col">Administradora</th>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Quantidade de Blocos</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $cond){
                ?>
                    
                    <tr data-id="<?=$cond['id']?>">
                        <td><?=$cond['nomeAdm']?></td>
                        <td><?=$cond['nomeCond']?></td>
                        <td><?=$cond['qtdeBlocos']?></td>
                        <td>Rua <?=$cond['logradouro']?>, <?=$cond['numero']?> - <?=$cond['bairro']?> - <?=$cond['cidade']?>/<?=$cond['estado']?> - <?=$cond['cep']?>
                        </td>
                        <td><a href="index.php?page=cadCondominio&id=<?=$cond['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$cond['id']?>" class="text-dark removerCond"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="5" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>

            </tbody>
        </table>
        <div class="col-sm-12">
            <?=$condominio->renderPagination($result['qtPaginas'])?>
        </div>
    </div>
</div>