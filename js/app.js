$(function(){

    $('#formCadastros').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = 'api/editaMorador.php';
            urlRedir =  '?page=consulta';
        } else{
            url = 'api/cadastraMorador.php';
            urlRedir = '?page=cadastro';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    });
    
    $('#listaMorador').on('click', '.removerMorador', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaMorador.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    function myAlert(tipo, mensagem, pai, url){
        url = (url == undefined) ? url == '' : url = url;
        componente = '<div class="alert alert-'+ tipo +'" role="alert">'+mensagem+'</div>';

        $(pai).prepend(componente);

        setTimeout(function(){
            $(pai).find('div.alert').remove();

            //vai redirecionar?
            if(tipo == 'success' && url){
                setTimeout(function(){
                    window.location.href = url;
                }, 500);
            }

        }, 1000)
    }

    $('#formCondominio').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = 'api/editaCondominio.php';
            urlRedir = '?page=consultaCond';
        } else{
            url = 'api/cadastraCond.php';
            urlRedir = '?page=cadCondominio';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaCond').on('click', '.removerCond', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formBlocos').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = 'api/editaBloco.php';
            urlRedir = '?page=consultaBlocos';
        } else{
            url = 'api/cadastraBloco.php';
            urlRedir = '?page=cadBlocos';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaBloco').on('click', '.removerBloco', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formUnidades').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = 'api/editaUnidade.php';
            urlRedir = '?page=consultaUni';
        } else{
            url = 'api/cadastraUnidade.php';
            urlRedir = '?page=cadUnidades';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaUni').on('click', '.removerUni', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formConselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = 'api/editaConselho.php';
            urlRedir = '?page=consultaConselho';
        } else{
            url = 'api/cadastraConselho.php';
            urlRedir = '?page=cadConselho';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaConselho').on('click', '.removerConselho', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formAdm').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = 'api/editaAdm.php';
            urlRedir = '?page=consultaAdm';
        } else{
            url = 'api/cadastraAdm.php';
            urlRedir = '?page=cadAdm';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    })

    $('#listaAdm').on('click', '.removerAdm', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaAdm.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    //chamar valores para selects filhos
    $('.fromCondominio').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: 'api/listBlocos.php',
            dataType:'json',
            type: 'POST',
            data: { id: selecionado },
            success: function(data){
                console.log(data);

                selectPopulation('.fromBloco',data.resultSet,'nomeBloco');
            }
        })
    })

    
    $('.fromBloco').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: 'api/listUnidades.php',
            dataType:'json',
            type: 'POST',
            data: { id: selecionado },
            success: function(data){
                selectPopulation('.fromUnidade',data.resultSet,'numUnidade');
            }
        })
    })
    
    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Select</option>';

        for (let i = 0; i < dados.length; i++) {
            estrutura += '<option value="' + dados[i].id + '">' + dados[i][field] + '</option>';
        }

        $(seletor).html(estrutura)
    }
});