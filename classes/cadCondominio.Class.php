<?

Class CadCondominio extends Administradora{
    protected $id;
    
    function __construct(){
        
    }

    function setCondominio($dadosCond){
        $values = '';
        $sql = 'INSERT INTO ap_condominio (';

        foreach($dadosCond as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getCondominio($id=null){
        $qry = 'SELECT * FROM v_listarcondominio';
        if($id){
            $qry .= ' WHERE id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function editCondominio($dadosCond){
        
        $sql = 'UPDATE ap_condominio SET ';

        foreach($dadosCond as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosCond['editar'];

        return $this->updateData($sql);
    }

    function deletaCondominio($id){

        $sql = 'DELETE FROM `ap_condominio` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

}

?>