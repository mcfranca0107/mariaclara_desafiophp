<?
Class CadUnidades extends cadBlocos{
    protected $id;

    function __construct(){
        
    }

    function setDadosUni($dadosUni){
        $values = '';
        $sql = 'INSERT INTO ap_unidades (';

        foreach($dadosUni as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getDadosUni($id=null){
        $qry = 'SELECT * FROM v_listarunidades';
        if($id){
            $qry .= ' WHERE id= '.$id;
        }
        return $this->listarData($qry,false);
    }

    function editUnidade($dadosUni){
        $sql = 'UPDATE ap_unidades SET ';

        foreach($dadosUni as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosUni['editar'];

        return $this->updateData($sql);
    }

    function deletaUnidade($id){
        $sql = 'DELETE FROM `ap_unidades` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

    function getUnidadeFromBloco($id){
        $qry = 'SELECT id, numUnidade FROM ap_unidades WHERE from_blocoUni = '.$id;
        return $this->listarData($qry);
    }

}

?>