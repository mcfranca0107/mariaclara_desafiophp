<?

Class conselhoFiscal extends CadCondominio{
    protected $id;

    function __construct(){

    }
    
    function setConselho($dados){
        $values = '';
        $sql = 'INSERT INTO ap_conselho (';

        foreach($dados as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getConselho($id=null){
        $qry = 'SELECT * FROM v_listarconselho';
        if($id){
            $qry .= ' WHERE id= '.$id;
        }
        return $this->listarData($qry,false);
    }

    function editConselho($dados){
        $sql = 'UPDATE ap_conselho SET ';

        foreach($dados as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaConselho($id){
        $sql = 'DELETE FROM `ap_conselho` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

}

?>