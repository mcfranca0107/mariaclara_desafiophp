<?
Class Administradora extends Dao{
    protected $id;

    function __construct(){

    }
    
    function setAdm($dados){
        $values = '';
        $sql = 'INSERT INTO ap_administradora (';

        foreach($dados as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql); 
    }

    function getAdm($id=null){
        $qry = 'SELECT * FROM v_listaradm';
        if($id){
            $qry .= ' WHERE id= '.$id;
        }
        return $this->listarData($qry,false);
    }

    function editAdm($dados){
        $sql = 'UPDATE ap_administradora SET ';

        foreach($dados as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaAdm($id){
        $sql = 'DELETE FROM `ap_administradora` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

}

?>