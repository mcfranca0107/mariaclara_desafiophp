<?

Class Cadastro extends CadUnidades{
    protected $id;

    function __construct(){

    }
    
    function setMorador($dados){
        $values = '';
        $sql = 'INSERT INTO ap_moradores (';

        foreach($dados as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql); 
    }

    function getMorador($id=null){
        $qry = 'SELECT * FROM v_listarmoradores';
        if($id){
            $qry .= ' WHERE id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }

    function editMorador($dados){
        $sql = 'UPDATE ap_moradores SET ';

        foreach($dados as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }

    function deletaMorador($id){
        $sql = 'DELETE FROM `ap_moradores` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

}

?>