<?
Class cadBlocos extends CadCondominio{
    protected $id;

    function __construct(){
        
    }

    function setBlocos($dadosBlocos){
        $values = '';
        $sql = 'INSERT INTO ap_blocos (';

        foreach($dadosBlocos as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getBlocos($id=null){
        $qry = 'SELECT * FROM v_listarblocos';
        if($id){
            $qry .= ' WHERE id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function editBloco($dadosBlocos){
        $sql = 'UPDATE ap_blocos SET ';

        foreach($dadosBlocos as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosBlocos['editar'];

        return $this->updateData($sql);
    }

    function deletaBloco($id){
        $sql = 'DELETE FROM `ap_blocos` WHERE `id`='.$id;

        return $this->deletar($sql);
    }

    function getBlocoFromCond($cond){
        $qry = 'SELECT id, nomeBloco FROM ap_blocos WHERE from_condBloco = '.$cond;
        return $this->listarData($qry);
    }

}

?>