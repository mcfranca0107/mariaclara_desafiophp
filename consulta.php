<?
$morador = new Cadastro();
$morador->pagination = 3;
$result = $morador->getMorador();
?>

<h1 class="text-center mb-4">Consulta dos moradores</h1>

<div class="row">
    <div class="col-12">
        
        <span class="float-right mr-4 mb-1">
            <a href="index.php?page=cadastro" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>

        <form class="form-inline my-2 my-lg-0" action="index.php" method="GET">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
        </form>
        
        <table class="table text-center" id="listaMorador">
            <thead>
                <tr>
                    <th scope="col">Condomínio</td>
                    <th scope="col">Bloco</td>
                    <th scope="col">Unidade</td>
                    <th scope="col">Nome</td>
                    <th scope="col">CPF</td>
                    <th scope="col">E-mail</td>
                    <th scope="col">Telefone</td>
                    <th scope="col" title="Data Cadastro"><i class="bi bi-calendar-plus" style="font-size: 25px;"></i></td>
                    <!-- <th scope="col" title="Data Última Edição"><i class="bi bi-calendar-check" style="font-size: 25px;"></i></td> -->
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>
            
            <tbody>

                <?
                    foreach($result['resultSet'] as $m){
                ?>

                    <tr data-id="<?=$m['id']?>">
                        <td><?=$m['nomeCond']?></td>
                        <td><?=$m['nomeBloco']?></td>
                        <td><?=$m['numUnidade']?></td>
                        <td><?=$m['nome']?></td>
                        <td><?=$m['cpf']?></td>
                        <td><?=$m['email']?></td>
                        <td><?=$m['telefone']?></td>
                        <td><?=dateFormat($m['dataCadastro'])?></td>
                        <!-- <td><?=dateFormat($m['dataEdicao'])?></td> -->
                        <td><a href="index.php?page=cadastro&id=<?=$m['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$m['id']?>" class="text-dark removerMorador"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>
                <tr>
                    <td colspan="9" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>
            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$morador->renderPagination($result['qtPaginas'])?>
        </div>
    </div>
</div>