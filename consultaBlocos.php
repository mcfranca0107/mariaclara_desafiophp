<?
$blocos = new CadBlocos();
$blocos->pagination=3;
$result = $blocos->getBlocos();
?>

<h1 class="text-center mb-4">Consulta de Blocos</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="index.php?page=cadBlocos" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>

        <table class="table text-center" id="listaBloco">
            <thead>
                <tr>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Nome do Bloco</th>
                    <th scope="col">Quantidade de Andares</th>
                    <th scope="col">Unidades por Andar</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>
                <?
                foreach($result['resultSet'] as $bloco){
                ?>
                    
                    <tr data-id="<?=$bloco['id']?>">
                        <td><?=$bloco['nomeCond']?></td>
                        <td><?=$bloco['nomeBloco']?></td>
                        <td><?=$bloco['qtdeAndares']?></td>
                        <td><?=$bloco['qtdeUni']?></td>
                        <td><a href="index.php?page=cadBlocos&id=<?=$bloco['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$bloco['id']?>" class="text-dark removerBloco"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="5" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>

            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$blocos->renderPagination($result['qtPaginas'])?>
        </div>
    </div>
</div>