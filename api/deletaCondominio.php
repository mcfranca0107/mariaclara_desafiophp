<?
require "../uteis.php";

$condominio = new CadCondominio();
$result = $condominio -> deletaCondominio($_POST['id']);

if($result){
    $totalRegistros = $condominio->getCondominio()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso.",
    );

    echo json_encode($result);

} else{

    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado.",
    );

    echo json_encode($result);
}
?>