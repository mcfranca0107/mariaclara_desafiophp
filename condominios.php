<div class="row">

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=cadAdm" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center" style="width:100%">Cadastrar Administradoras</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=consultaAdm" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center" style="width:100%">Consultar Administradoras</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=cadConselho" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar conselho fiscal</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=consultaConselho" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar conselho fiscal</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=cadCondominio" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar condomínios</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=consultaCond" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar condomínios</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=cadBlocos" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar blocos</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=consultaBlocos" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar blocos</a>
    </div>

    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=cadUnidades" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Cadastrar unidades</a>
    </div>
    <div class="col-12 col-sm-6 d-inline-block">
        <a href="index.php?page=consultaUni" class="bg-dark text-decoration-none text-white btn btn-lg btn-block text-center mt-3" style="width:100%">Consultar unidades</a>
    </div>
</div>
