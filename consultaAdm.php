<?
$administradora = new Administradora();
$administradora->pagination=3;
$result = $administradora->getAdm();
?>

<h1 class="text-center mb-4">Consulta das Administradoras</h1>

<div class="row">
    <div class="col-12">
        <span class="float-right mr-4 mb-1">
            <a href="index.php?page=cadAdm" class="text-dark"><i class="bi bi-patch-plus" style="font-size: 2rem;"></i></a>
        </span>
        <table class="table text-center" id="listaAdm">
            <thead>
                <tr>
                    <th scope="col">Administradora</th>
                    <th scope="col">Quantidade de Condomínios</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $adm){
                ?>
                    
                    <tr data-id="<?=$adm['id']?>">
                        <td><?=$adm['nomeAdm']?></td>
                        <td><?=$adm['qtdeCond']?></td>
                        <td><a href="index.php?page=cadAdm&id=<?=$adm['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td><a href="#" data-id="<?=$adm['id']?>" class="text-dark removerAdm"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="3" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>

            </tbody>
        </table>

        <div class="col-sm-12">
            <?=$administradora->renderPagination($result['qtPaginas'])?>
        </div>
    </div>
</div>