<?
error_reporting('E_FATAL | E_PARSE' );
require "uteis.php";
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Gestão de Clientes</title>
</head>

<body>

    <nav class="navbar navbar-expand-lg bg-dark mb-4">
        <a class="navbar-brand" href="#"><i class="bi bi-person-bounding-box ml-4" style="font-size: 3rem; color: #fff"></i> <span class="text-white font-weight-bold pl-3"> GESTÃO DE CLIENTES </span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end mr-5" id="navbarNav">
            <ul class="navbar-nav float-right">
                <li class="nav-item">
                    <a class="nav-link text-white" href="index.php?page=inicio">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white pl-4" href="index.php?page=cadastro">Cadastrar Morador</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white pl-4" href="index.php?page=consulta">Moradores</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white pl-4" href="index.php?page=condominios">Condomínios</a>
                </li>
            </ul>
        </div>
    </nav>

    <main class="container">
    <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "inicio.php";
            break;
            
            default:
                require $_GET['page'].'.php';
            break;
        }
    ?>
    </main>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js?v=<?=rand(0,9999)?>"></script>
</body>

</html>