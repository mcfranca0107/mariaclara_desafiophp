<?    

$listaMorador = new Cadastro();
$result = $listaMorador->getMorador($_GET['id']);
$morador = $result['resultSet'];
// legivel($morador);

?>

<h1 class="text-center mb-5">Cadastros dos moradores</h1>
<div class="row">
    <div class="col-12">
        <form action="" method="post" id="formCadastros">
            <div class="form-group col-md-6">
                <label for="name">Nome*</label>
                <input type="text" name="nome" class="form-control" id="name" aria-describedby="name" value="<?=$morador['nome']?>" required>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="cpf">CPF*</label>
                <input type="text" name="cpf" class="form-control" id="cpf" placeholder="000.000.000-00" value="<?=$morador['cpf']?>" required>
            </div>
            
            <div class="form-group col-12 col-md-6">
                <label for="telefone">Telefone</label>
                <input type="text" name="telefone" class="form-control" id="telefone" aria-describedby="telefone" value="<?=$morador['telefone']?>" required>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="email">E-mail*</label>
                <input type="text" name="email" class="form-control" id="email" aria-describedby="email" value="<?=$morador['email']?>" required>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="from_condominio">Condomínio*</label>
            
                <select name="from_condominio" id="from_condominio" class="custom-select fromCondominio">
                    <option value="">Select</option>
                    <?
                    $condominio = new CadCondominio();
                    $result = $condominio->getCondominio();
                    $cond = $result['resultSet'];

                    foreach($cond as $ch=>$valor){?>
                        <option value="<?=$valor['id']?>" <?=($valor['id'] == $morador['from_condominio'] ? 'selected' : '')?>> <?=$valor['nomeCond']?></option> 
                    <?}?>

                </select>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="from_bloco">Bloco*</label>
                
                <select name="from_bloco" id="from_bloco" class="custom-select fromBloco">
                    <?if($_GET['id']){
                        $blocos = $listaMorador->getBlocoFromCond($morador['from_condominio']);
                        foreach($blocos['resultSet'] as $bloco){?>
                            <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $morador['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                    <?}}?>
                </select>
            
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="from_unidade">Unidade*</label>
                
                <select name="from_unidade" id="from_unidade" class="custom-select fromUnidade">
                    <?if($_GET['id']){
                        $unidades = $listaMorador->getUnidadeFromBloco($morador['from_bloco']);
                        foreach($unidades['resultSet'] as $uni){?>
                            <option value="<?=$uni['id']?>"<?=($uni['id'] == $morador['from_unidade'] ? 'selected' : '')?>><?=$uni['numUnidade']?></option>
                    <?}}?>
                </select>

            </div>

            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <button type="submit" class="btn btn-dark btnEnviar col-12 col-sm-1 ml-3 mb-3">Enviar</button>
            <a href="index.php?page=consulta" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar cadastros"><i class="bi bi-journal-text" style="font-size: 2rem"></i></a>
        </form>
    </div>
</div>